# HelloWorld React Native
	
![image](assets/cover1.png)

## INICIANDO

Para iniciar, tu requieres tener instalado laragon (recomendado), ademas de visual studio Code. 

## INSTALACION
Primeramente descarga el proyecto y luego ejecuta los siguientes comandos:
```
cd helloworld-react-native
npm install
npm start
```
## EMULADOR
Prodras ver el resultado en un emulador android o la web.
![image](assets/options.png)

## PROYECTO
Si te gusto el proyecto, dale un like.
