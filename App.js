import { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends Component {
  render() {
    return (
    <View style={styles.container}>
      <Text style={styles.box0}>Hello world!!! UPDS</Text>
      <View style={{flexDirection:'row'}}>
        
        <Text style={styles.box1}>Box one</Text>
        <Text style={styles.box1}>Box two</Text>
      </View>
      <View style={{flexDirection:'column'}}>
        <Text style={styles.box2}>Box one</Text>
        <Text style={styles.box2}>Box two</Text>
      </View>
      <Text style={styles.subtitle}>Jaime Zambrana </Text>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#a1c468',
    alignItems: 'center',
    justifyContent: 'center',    
  },
  box0:{    
    fontSize:35,    
    padding:30,
    margin:10
  },
  box1:{
    backgroundColor:'#83b029',
    fontSize:20,
    borderStyle: 'dashed',    
    borderColor: 'red',
    borderWidth: 5,    
    padding:30,
    margin:10
  },
  box2:{
    backgroundColor:'#8f8323',
    fontSize:20,
    borderStyle: 'dashed',    
    borderColor: 'red',
    borderWidth: 5,    
    padding:30,
    margin:10
  },
  subtitle:{    
    justifyContent:'flex-end',
    fontSize:25,    
  }
});
